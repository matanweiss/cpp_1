#include <iostream>
#include <cstdio>
#include "Stack.h"


/*
function intelizes the stack's node
input: the stack
output: none
*/
void initStack(Stack* s)
{
	s->top = new node;
}


/*
function cleans the stack 
input: the stack
output: none
*/
void cleanStack(Stack* s)
{
	while (s->top != nullptr) //removing all of the nodes in the list that is in the stack
	{
		remove(&s->top);
	}
}
 
/*
function adds a element to the stack
input: the stack and an element
output: none
*/
void push(Stack* s, unsigned int element)
{
	add(&s->top, element);
}


/*
function removes and returnes the top value of the stack
input: the stack
output: the value of the removed element,-1 if the stack is empty
*/
int pop(Stack* s) 
{
	int val = -1;

	if (s->top != nullptr || s->top->next != NULL)
	{
		val = s->top->data;
		remove(&s->top);
	}
	
	return val;	
}