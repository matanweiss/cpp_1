#include <iostream>
#include <cstdio>
#include "Stack.h"

/*
function reverses an array
input: an array and his length
output: none
*/
void reverse(int* nums, unsigned int size)
{
	Stack *s = new Stack;
	for (int i = 0; i < size; i++) // inserting the array to a string
	{
		push(s, nums[i]);
	}

	for (int j = 0; j < size; j++)
	{
		nums[j] = pop(s); // popping the values of the stck to the array
	}
}

/*
function reverses a arry of input ints
input: none
output: the array after reverse
*/
int* reverse10()
{
	int *arr = new int[10];
	for (int i = 0; i < 10; i++)
	{
		std::cin >> arr[i];
	}
	reverse(arr, 10);
	return arr;
}