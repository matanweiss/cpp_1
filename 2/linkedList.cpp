#include <iostream>
#include <cstdio>
#include "linkedList.h"

/*
function adds a new node to the start of the list
input: a pointer to the first node int the list and a value
output: none
*/
void add(node**first, unsigned int value)
{
	node *newNode = new node; //creating a new node
	newNode->data = value; // inserting data
	newNode->next = *first; // setting the next to the first node in the list
	*first = newNode; // setting the value of the pointer to the new node
}

/*
function removes the first node from a linked list
input: a pointer to a linked list
output: none
*/
void remove(node**first)
{
	node *temp = *first;
	*first = (*first)->next; // changing the first node in the list to the sec node
	delete temp;
}