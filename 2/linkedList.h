#pragma once
#include <cstdio>

typedef struct node node;
struct node {
	unsigned int data;
	node* next = NULL;
};

void add(node**first, unsigned int value);

void remove(node**first);
