#pragma once
#include "linkedList.h"

typedef struct Stack Stack;
struct Stack {
	node* top = NULL;
};

void push(Stack* s, unsigned int element);
int pop(Stack* s); // Return -1 if stack is empty

void initStack(Stack* s);
void cleanStack(Stack* s);
