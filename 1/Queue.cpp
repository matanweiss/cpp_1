#include "Queue.h"
#include <iostream>


/*
function initializes the queue with a given size
input: a queue and a given size
output: none
*/
void initQueue(queue* q, unsigned int size)
{
	q->arr = new unsigned int[size]; // initializing array by size
	q->max = size; // setting the max size
	for (int i = 0; i < size; i++)  // intializing all of the data in the array to 0
	{
		q->arr[i] = 0;
	}
}

/*
function inserts a value to a queue
input: a queue and a value
output: none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->currIdx < q->max) // checking if the queue is full
	{
		q->arr[(q->currIdx)] = newValue; // inserting the new value
		q->currIdx++;
	}
}


/*
function frees a queue
input: a queue
output: none
*/
void cleanQueue(queue* q)
{
	delete[]q->arr;
}

/*
function delets the first value in the queue and returnes it
input: a queue
output: the first value in the queue
*/
int dequeue(queue* q)
{
	int val = q->arr[0]; // saving the value
	for (int i = 0; i < q->currIdx; i++) // mooving all of the remaining to the left
	{
		q->arr[i] = q->arr[i + 1];
	}
	q->currIdx--; // decreasing the curr idx
	return val; //returning the value
}